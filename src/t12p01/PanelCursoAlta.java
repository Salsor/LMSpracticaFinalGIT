package t12p01;

import javax.swing.JOptionPane;
import modelo.ConexionBD;
import modelo.Curso;

public class PanelCursoAlta extends javax.swing.JPanel {

    private ConexionBD bd;

    public PanelCursoAlta(ConexionBD bd) {
        initComponents();
        this.bd = bd;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        PanelCursoAlta_Label_altadecurso = new javax.swing.JLabel();
        PanelCursoAlta_Label_id = new javax.swing.JLabel();
        PanelCursoAlta_Label_titulo = new javax.swing.JLabel();
        PanelCursoAlta_Label_horas = new javax.swing.JLabel();
        PanelCursoAlta_TextField_id = new javax.swing.JTextField();
        PanelCursoAlta_TextField_titulo = new javax.swing.JTextField();
        PanelCursoAlta_TextField_horas = new javax.swing.JTextField();
        PanelCursoAlta_but_Aceptar = new javax.swing.JButton();
        PanelCursoAlta_but_cancelar = new javax.swing.JButton();

        PanelCursoAlta_Label_altadecurso.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        PanelCursoAlta_Label_altadecurso.setText("ALTA DE CURSO");

        PanelCursoAlta_Label_id.setText("Identificador");

        PanelCursoAlta_Label_titulo.setText("Titulo:");

        PanelCursoAlta_Label_horas.setText("Horas:");

        PanelCursoAlta_but_Aceptar.setText("Aceptar");
        PanelCursoAlta_but_Aceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PanelCursoAlta_but_AceptarActionPerformed(evt);
            }
        });

        PanelCursoAlta_but_cancelar.setText("Cancelar");
        PanelCursoAlta_but_cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PanelCursoAlta_but_cancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(157, 157, 157)
                        .addComponent(PanelCursoAlta_Label_altadecurso))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(PanelCursoAlta_Label_id)
                                    .addComponent(PanelCursoAlta_Label_titulo)
                                    .addComponent(PanelCursoAlta_Label_horas))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(PanelCursoAlta_TextField_id, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
                                    .addComponent(PanelCursoAlta_TextField_titulo)
                                    .addComponent(PanelCursoAlta_TextField_horas)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(PanelCursoAlta_but_cancelar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(PanelCursoAlta_but_Aceptar)))))
                .addContainerGap(31, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addComponent(PanelCursoAlta_Label_altadecurso)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(PanelCursoAlta_Label_id)
                    .addComponent(PanelCursoAlta_TextField_id, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(PanelCursoAlta_Label_titulo)
                    .addComponent(PanelCursoAlta_TextField_titulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(PanelCursoAlta_Label_horas)
                    .addComponent(PanelCursoAlta_TextField_horas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 42, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(PanelCursoAlta_but_Aceptar)
                    .addComponent(PanelCursoAlta_but_cancelar))
                .addGap(71, 71, 71))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void PanelCursoAlta_but_AceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PanelCursoAlta_but_AceptarActionPerformed
        if (PanelCursoAlta_TextField_id.getText().equals("")
                || PanelCursoAlta_TextField_titulo.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Faltan campos obligatorios\n", "Error", JOptionPane.WARNING_MESSAGE);
        } else {
            try {
                Curso c = new Curso();
                String horas, id;
                id = PanelCursoAlta_TextField_id.getText();
                horas = PanelCursoAlta_TextField_horas.getText();
                if (!horas.equals("")) {
                    c.setHoras(Double.parseDouble(horas));
                }
                c.setId(Integer.parseInt(id));
                c.setTitulo(PanelCursoAlta_TextField_titulo.getText());
                c.altaCurso(bd);
                JOptionPane.showMessageDialog(this, "Alta de curso correcta. ID: " + c.getId(),
                        "Base de datos", JOptionPane.INFORMATION_MESSAGE);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, "Error!!\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_PanelCursoAlta_but_AceptarActionPerformed

    private void PanelCursoAlta_but_cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PanelCursoAlta_but_cancelarActionPerformed
        this.setVisible(false);
        PanelCursoAlta_TextField_id.setText("");
        PanelCursoAlta_TextField_titulo.setText("");
        PanelCursoAlta_TextField_horas.setText("");
    }//GEN-LAST:event_PanelCursoAlta_but_cancelarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel PanelCursoAlta_Label_altadecurso;
    private javax.swing.JLabel PanelCursoAlta_Label_horas;
    private javax.swing.JLabel PanelCursoAlta_Label_id;
    private javax.swing.JLabel PanelCursoAlta_Label_titulo;
    private javax.swing.JTextField PanelCursoAlta_TextField_horas;
    private javax.swing.JTextField PanelCursoAlta_TextField_id;
    private javax.swing.JTextField PanelCursoAlta_TextField_titulo;
    private javax.swing.JButton PanelCursoAlta_but_Aceptar;
    private javax.swing.JButton PanelCursoAlta_but_cancelar;
    // End of variables declaration//GEN-END:variables
}
